import os
import requests
from datetime import datetime, timedelta
from random import randint

# Get (or set if missiong) environment variables
site=os.environ.get('site')
if site==None:
    os.environ['site']=str(input("site: "))
    site=os.environ['site']

mentorsList="MediaWiki:GrowthMentors.json"

userTalkNsName=os.environ.get('userTalkNsName')
if userTalkNsName==None:
    os.environ['userTalkNsName']=str(input("userTalkNsName: "))
    userTalkNsName=os.environ['userTalkNsName']

userNsName=os.environ.get('userNsName')
if userNsName==None:
    os.environ['userNsName']=str(input("userNsName: "))
    userNsName=os.environ['userNsName']

requestsPage=os.environ.get('requestsPage')
if requestsPage==None:
    os.environ['requestsPage']=str(input("requestsPage: "))
    requestsPage=os.environ['requestsPage']

loginUsername=os.environ.get('loginUsername')
if loginUsername==None:
    os.environ['loginUsername']=str(input("loginUsername: "))
    loginUsername=os.environ['loginUsername']

loginPassword=os.environ.get('loginPassword')
if loginPassword==None:
    os.environ['loginPassword']=str(input("loginPassword: "))
    loginPassword=os.environ['loginPassword']

userAgent=os.environ.get('userAgent')
if userAgent==None:
    os.environ['userAgent']=str(input("userAgent: "))
    userAgent=os.environ['userAgent']

# Script variables
summarytxt="Bot: assegnazione automatica della richiesta ad un tutor attivo"
appendtxt="\n:''Messaggio automatico'': questa richiesta non è ancora stata  evasa. Viene assegnata automaticamente a {{at|$1}}--~~~~" #$1 = new assigned mentor
date=[datetime.today()-timedelta(100), datetime.today()-timedelta(1)] #Array with initial and final date to check
inactivityDaysMentors=0 #Max N of inactivity days a mentor can have to be selected
runsOnMidnight=1 #1 if true, 0 if false; increase by 1 inactivity days

# Setup API requests
S = requests.Session()
URL = "https://"+site+".wikipedia.org/w/api.php"
headers = {'User-Agent': userAgent}

#Check if a mentor has been active in the past inactivityDays days
def isActive(mentor, inactivityDays):
    PARAMS={
        "action": "query",
        "format": "json",
        "list": "usercontribs",
        "ucend": datetime.strftime(datetime.today()-timedelta(inactivityDays), '%Y-%m-%d')+"T00:00:00.000Z",
        "ucuser": mentor
    }
    edits=S.get(url=URL, params=PARAMS, headers=headers).json()['query']['usercontribs']

    PARAMS={
        "action": "query",
        "format": "json",
        "list": "logevents",
        "leend": datetime.strftime(datetime.today()-timedelta(inactivityDays), '%Y-%m-%d')+"T00:00:00.000Z",
        "leuser": mentor,
        "lelimit": "1"
    }
    hasLogs=len(S.get(url=URL, params=PARAMS, headers=headers).json()['query']['logevents']) #returns 0 or 1

    return len(edits)>0 or hasLogs == 1

#Get mentors list
mentors=[]
PARAMS = {
	"action": "parse",
	"format": "json",
	"page": mentorsList,
	"prop": "wikitext",
	"formatversion": "2"
}
R=S.get(url=URL, params=PARAMS, headers=headers).json()['parse']['wikitext']
mentors=[]

#Get a list of all the NUMBERS contained between "\"" and "\"" in the wikitext
parsedMentorsIds=R.split("\"")
parsedMentorsIds=parsedMentorsIds[1::2]

#If is not a number, remove from R
for i in range(len(parsedMentorsIds)-1, -1, -1):
    try:
        parsedMentorsIds[i]=int(parsedMentorsIds[i])
    except:
        parsedMentorsIds.pop(i)

for id in parsedMentorsIds:
    #API call to get mentor's usernamee
    PARAMS = {
        "action": "query",
        "format": "json",
        "list": "users",
        "formatversion": "2",
        "ususerids": id
    }
    R=S.get(url=URL, params=PARAMS, headers=headers).json()['query']['users'][0]['name']
    mentors+=[R]

#Request list in the dates range
PARAMS={
	"action": "query",
	"format": "json",
	"list": "recentchanges",
	"continue": "-||",
	"formatversion": "2",
	"rcstart": datetime.strftime(date[0], '%Y-%m-%d')+"T00:00:00.000Z",
	"rcend": datetime.strftime(date[1], '%Y-%m-%d')+"T00:00:00.000Z",
	"rcdir": "newer",
	"rctitle": requestsPage,
	"rcprop": "ids|user",
	"rclimit": "500",
	"rcgeneraterevisions": 1,
	"rcslot": "main"
}
R=S.get(url=URL, params=PARAMS, headers=headers)
listQuestions=R.json()['query']['recentchanges']



for question in listQuestions:
    #Get text
    PARAMS={
        "action": "parse",
        "format": "json",
        "oldid": question['revid'],
        "prop": "sections",
        "formatversion": "2"
    }
    R=S.get(url=URL, params=PARAMS, headers=headers)
    section=len(R.json()['parse']['sections'])

    PARAMS={
        "action": "parse",
        "format": "json",
        "oldid": question['revid'],
        "prop": "wikitext",
        "section": section,
        "utf8": 1,
        "ascii": 1,
        "formatversion": "2"
    }
    R=S.get(url=URL, params=PARAMS, headers=headers)

    oldtext=R.json()['parse']['wikitext']

    #Check if it is a new request (new section)
    if "\n:" in oldtext:
        continue

    #Check if the request has been answered
    mentee=question['user']

    PARAMS={
	"action": "parse",
	"format": "json",
	"page": requestsPage,
	"prop": "sections|wikitext",
	"formatversion": "2"
    }
    R=S.get(url=URL, params=PARAMS, headers=headers)
    cursection=None
    for i in range(len(R.json()['parse']['sections']), 1, -1):
        if mentee in R.json()['parse']['sections'][i-1]['line']:
            cursection=i
            break
    if cursection==None:
        #The request has been undid or archived
        continue

    PARAMS={
        "action": "parse",
        "format": "json",
        "prop": "wikitext",
        "page": requestsPage,
        "section": cursection,
        "utf8": 1,
        "ascii": 1,
        "formatversion": "2"
    }
    R=S.get(url=URL, params=PARAMS, headers=headers)
    curtext=R.json()['parse']['wikitext']

    print("\n\n"+mentee) #debug

    if curtext != oldtext:
        continue

    print("THE QUESTION HASN'T BEEN ANSWERED JET") #log

    #Assign the request to a mentor
    PARAMS_0 = {
        "action": "query",
        "meta": "tokens",
        "type": "login",
        "format": "json"
    }
    R = S.get(url=URL, params=PARAMS_0, headers=headers)
    DATA = R.json()

    LOGIN_TOKEN = DATA['query']['tokens']['logintoken']

    PARAMS_1 = {
        "action": "login",
        "lgname": loginUsername,
        "lgpassword": loginPassword,
        "lgtoken": LOGIN_TOKEN,
        "format": "json"
    }

    R = S.post(URL, data=PARAMS_1, headers=headers)

    PARAMS_2 = {
        "action": "query",
        "meta": "tokens",
        "format": "json"
    }

    R = S.get(url=URL, params=PARAMS_2, headers=headers)
    DATA = R.json()

    CSRF_TOKEN = DATA['query']['tokens']['csrftoken']

    PARAMS_3 = {
        "action": "edit",
        "title": requestsPage,
        "section": cursection,
        "token": CSRF_TOKEN,
        "summary": summarytxt,
        "format": "json",
        "appendtext": appendtxt.replace("$1", mentors[randint(0,len(mentors)-1)])
    }

    R = S.post(URL, data=PARAMS_3, headers=headers)
    DATA = R.json()
